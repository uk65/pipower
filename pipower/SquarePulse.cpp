/**
 * \class SquarePulse
 * \brief SquarePulse steuert den Signalgenerator der Steuereinheit.
 *
 * Der Signalgenerator arbeitet mit dem 16-bit Timer (timer1) des
 * Atmel ATmega328p Microcontrollers. Der Timer wird im Fast PWM
 * Modus 14 nicht-invertierend betrieben.
 *
 * Der Timer setzt den Ausgang PB1 beim Nulldurchgang auf HIGH, zählt
 * bis zur im Register ICR1 hinterlegten Obergrenze. Wird der in OCR1A
 * gespeicherte Wert erreicht, wird PB1 auf LOW gesetzt.
 * Die Kombinationen aus Obergrenze und Prescaler werden so gewählt,
 * das sich möglichst geringe Abweichungen von den Sollwerten ergeben.
 *
 * Die Frequenz des Signalgenerators wird mit den linken vier DIP-Schaltern
 * der verbauten Schaltereinheit eingestellt. Folgende Frequenzen sind möglich:
 *
 * <table>
 *   <tr>
 *     <td></td>
 *     <td colspan=4> DIP-Schalter </td>
 *   </tr>
 *   <tr>
 *     <td>Frequenz Hz </td>
 *     <td>1</td>
 *     <td>2</td>
 *     <td>3</td>
 *     <td>4</td>
 *   </tr>
 *   <tr>
 *     <td>0,25</td>
 *     <td>0</td>
 *     <td>0</td>
 *     <td>0</td>
 *     <td>1</td>
 *   </tr>
 *   <tr>
 *     <td>0,5</td>
 *     <td>0</td>
 *     <td>0</td>
 *     <td>1</td>
 *     <td>0</td>
 *   </tr>
 *   <tr>
 *     <td>1</td>
 *     <td>0</td>
 *     <td>0</td>
 *     <td>1</td>
 *     <td>1</td>
 *   </tr>
 *   <tr>
 *     <td>5</td>
 *     <td>0</td>
 *     <td>1</td>
 *     <td>0</td>
 *     <td>0</td>
 *   </tr>
 *   <tr>
 *     <td>10</td>
 *     <td>0</td>
 *     <td>1</td>
 *     <td>0</td>
 *     <td>1</td>
 *   </tr>
 *   <tr>
 *     <td>25</td>
 *     <td>0</td>
 *     <td>1</td>
 *     <td>1</td>
 *     <td>0</td>
 *   </tr>
 *   <tr>
 *     <td>50</td>
 *     <td>0</td>
 *     <td>1</td>
 *     <td>1</td>
 *     <td>1</td>
 *   </tr>
 *   <tr>
 *     <td>100</td>
 *     <td>1</td>
 *     <td>0</td>
 *     <td>0</td>
 *     <td>0</td>
 *   </tr>
 *   <tr>
 *     <td>200</td>
 *     <td>1</td>
 *     <td>0</td>
 *     <td>0</td>
 *     <td>1</td>
 *   </tr>
 *   <tr>
 *     <td>500</td>
 *     <td>1</td>
 *     <td>0</td>
 *     <td>1</td>
 *     <td>0</td>
 *   </tr>
 *   <tr>
 *     <td>1000</td>
 *     <td>1</td>
 *     <td>0</td>
 *     <td>1</td>
 *     <td>1</td>
 *   </tr>
 * </table>
 *
 * Weitehin kann das Tastverhältnis (duty cycle) des Rechtecksignals eingestellt werden:
 *
 * <table>
 *   <tr>
 *     <td></td>
 *     <td colspan=4> DIP-Schalter </td>
 *   </tr>
 *   <tr>
 *     <td>Tastverhältnis %</td>
 *     <td>5</td>
 *     <td>6</td>
 *   </tr>
 *   <tr>
 *     <td>10</td>
 *     <td>0</td>
 *     <td>0</td>
 *   </tr>
 *   <tr>
 *     <td>20</td>
 *     <td>0</td>
 *     <td>1</td>
 *   </tr>
 *   <tr>
 *     <td>30</td>
 *     <td>1</td>
 *     <td>0</td>
 *   </tr>
 *   <tr>
 *     <td>50</td>
 *     <td>1</td>
 *     <td>1</td>
 *   </tr>
 * </table>
 */

#include <avr/io.h>
#include <avr/interrupt.h>

#include "SquarePulse.h"
#include "Hardware.h"

SquarePulse::SquarePulse()
{
    DDRB  |=  _BV(PINB1);
    PORTB &= ~_BV(PINB1);
    
    // Der Arduino Bootloader initialisiert die Timer, also alles auf 0.
    TCCR1A = 0;
    TCCR1B = 0;
    TCCR1A |= _BV(COM1A1);      // None-inverted mode (HIGH at bottom, LOW on Match)
    TCCR1A |= _BV(WGM11);       // Mode 14: Fast PCM, Top ICR1
    TCCR1B |= _BV(WGM13) | _BV(WGM12);
//    TIMSK1 |= _BV(OCIE1A);
}

void SquarePulse::setGenerator()
{
    /*
    static uint8_t prevPinB = 0;
    
    // PWM Status hat sich geändert
    if ((PINB & _BV(PIN_DIL_1000)) != (prevPinB & _BV(PIN_DIL_1000)))
    {
        // PWM aus
        if (!(PINB & _BV(PIN_DIL_1000)))
        {
            TCCR2B &= ~(_BV(CS12) | _BV(CS11) | _BV(CS10));
            prevPinB = PINB;
            return;
        }
    }
    
    uint8_t prevPwmMode = (prevPinB ^ 0b00000111);
    uint8_t pwmMode     = (PINB     ^ 0b00000111);

    prevPinB = PINB;

    if (pwmMode == prevPwmMode)
        return;
    */
    
    TCCR1B &= ~(_BV(CS12) | _BV(CS11) | _BV(CS10));
    
    uint16_t mode = 0x0011;
    uint8_t  duty = 0x11;

    switch (mode)
    {
        case 0x0001:            // 0,25 Hz
            ICR1    = 62500;
            TCCR1B |= _BV(CS12) | _BV(CS10);
            break;

        case 0x0010:            // 0,5 Hz
            ICR1    = 31250;
            TCCR1B |= _BV(CS12) | _BV(CS10);
            break;

        case 0x0011:            // 1 Hz
            ICR1    = 62500;
            TCCR1B |= _BV(CS12);
            break;

        case 0x0100:            // 5 Hz
            ICR1    = 50000;
            TCCR1B |= _BV(CS11) | _BV(CS10);
            break;

        case 0x0101:            // 10 Hz
            ICR1    = 25000;
            TCCR1B |= _BV(CS11) | _BV(CS10);
            break;

        case 0x0110:            // 25 Hz
            ICR1    = 10000;
            TCCR1B |= _BV(CS11) | _BV(CS10);
            break;

        case 0x0111:            // 50 Hz
            ICR1    = 40000;
            TCCR1B |= _BV(CS11);
            break;

        case 0x1000:            // 100 Hz
            ICR1    = 20000;
            TCCR1B |= _BV(CS11);
            break;

        case 0x1001:            // 200 Hz
            ICR1    = 10000;
            TCCR1B |= _BV(CS11);
            break;

        case 0x1010:            // 500 Hz
            ICR1    = 32000;
            TCCR1B |= _BV(CS10);
            break;

        case 0x1011:            // 1000 Hz
            ICR1    = 16000;
            TCCR1B |= _BV(CS10);
            break;
    }
    
    switch (duty)
    {
        case 0x00:
            OCR1A = ICR1 * 0.1;
            break;
        
        case 0x01:
            OCR1A = ICR1 * 0.2;
            break;
        
        case 0x10:
            OCR1A = ICR1 * 0.3;
            break;
        
        case 0x11:
            OCR1A = ICR1 * 0.5;
            break;
    }
}