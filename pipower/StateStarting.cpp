/**
 * \class StateStarting
 * \brief StateStarting repräsentiert den Start des Raspberry Pi.
 *
 * Hier wird auf Lebenszeichen vom Raspberry Pi gewartet.
 * Werden diese erkannt, wird zum Status \ref StateRunning gewechselt.
 * Werden nach einer definierten Zeitspanne vom Raspberry Pi keine
 * Lebenszeichen erkannt, wird davon ausgegangen, daß der Start
 * fehlgeschlagen ist. Es wird in den Status \ref StateStandBy
 * gewechselt.
 */

#include "StateStarting.h"
#include "Hardware.h"

/**
 * \brief Beim Aktivieren des Status StateStarting wird durch den Aufruf
 *        der Basisklassenmethode \ref State::entryAction() der 8-bit
 *        Zähler auf 0 gesetzt.
 *        Außerdem wird die Strombersorgung des Raspberry Pi eingeschaltet.
 *        Die Status-LED blinkt.
 */
void StateStarting::entryAction()
{
    State::entryAction();
    
    LED::setMode(LED::LedMode::Blinking);
    Power::setPower(true);
}

/**
 * \brief Durch Aufruf der Basisklassenmethode \ref State::tickAction()
 *        wird der 8-bit Zähler inkrementiert.
 */
void StateStarting::tickAction()
{
    State::tickAction();
    
    if (m_ticks > 5)
        PiSimulation::setSimulation(true);
}
