#ifndef __STATE_REBOOT_H__
#define __STATE_REBOOT_H__

#include "State.h"

class StateReboot : public State
{
public:

    virtual void entryAction();
    virtual void tickAction();
};

#endif // __STATE_REBOOT_H__
