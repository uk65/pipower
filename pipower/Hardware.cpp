
#include <util/delay.h>

#include "Defines.h"
#include "Hardware.h"

//-----------------------------------------------------------------------------

uint8_t g_ledMode = LED::LedMode::Off;

void LED::tick100ms(const uint8_t tick)
{
    switch (tick)
    {
        case 0:
        {
            switch (g_ledMode)
            {
                case LED::LedMode::Off: 
                    PORTD &= ~_BV(PIN_LED); 
                    break;
                case LED::LedMode::On:
                case LED::LedMode::Flashing:
                case LED::LedMode::Blinking:
                    PORTD |= _BV(PIN_LED);
                    break;
            }
            break;
        }
        
        case 1:
        {
            if (g_ledMode == LED::LedMode::Flashing)
                PORTD &= ~_BV(PIN_LED);
            break;            
        }
                
        case 5:
        {
            if (g_ledMode == LED::LedMode::Blinking)
                PORTD &= ~_BV(PIN_LED);
            break;                        
        }
    }
}

void LED::setMode(const LED::LedMode ledMode)
{
    g_ledMode = ledMode;
}

//-----------------------------------------------------------------------------

void DebugLED::flash(const uint8_t count)
{
    for (uint8_t i = 0; i < count; i++)
    {
        PORTB |= _BV(PIN_LED_BOARD);
        _delay_ms(100);
        PORTB &= ~_BV(PIN_LED_BOARD);
        _delay_ms(100);
    }
}

//-----------------------------------------------------------------------------

void Power::setPower(const bool powerOn)
{
    if (powerOn)
        PORTD |= _BV(PIN_POWER);
    else
        PORTD &= ~_BV(PIN_POWER);
}

//-----------------------------------------------------------------------------

uint8_t g_piSignals = 0; 

void PiSignal::tick100ms(const uint8_t tick)
{
    // 1 Signal      0ms -  200ms
    // 2 Signale     0ms -  200ms
    //             800ms - 1000ms
    
    switch (tick)
    {
        case 0:
        {            
            if (g_piSignals > 0)
                PORTD &= ~_BV(PIN_TO_PI);
            break;
        }
        
        case 2:
        {
            if (g_piSignals > 0)
            {
                PORTD |= _BV(PIN_TO_PI);
                g_piSignals--;
            }
            break;            
        }
        
        case 8:
        {
            if (g_piSignals > 0)
                PORTD &= ~_BV(PIN_TO_PI);
            break;                        
        }
            
        case 10:
        {
            if (g_piSignals > 0)
            {
                PORTD |= _BV(PIN_TO_PI);
                g_piSignals--;
            }
            break;                        
        }
    }    
}

void PiSignal::setSignal(const uint8_t piSignals)
{
    g_piSignals = piSignals;
}

//-----------------------------------------------------------------------------

uint8_t g_piSimulation = false; 

void PiSimulation::tick100ms(const uint8_t tick)
{
    // 5Hz Ticks 100ms / Pause 100ms
    if (g_piSimulation)
        PORTB ^= _BV(PIN_PI_PULSE);
    else
        PORTB &= ~_BV(PIN_PI_PULSE);
}

void PiSimulation::setSimulation(const bool piSimulation)
{
    g_piSimulation = piSimulation;
}

//-----------------------------------------------------------------------------

void DipSwitch::init()
{
    // Ausgänge
    DDRB |= _BV(PIN_SPI_SCK) | _BV(PIN_SPI_SS);

    // SPI-Initialisierung (master mode)
    // keine Interrupts, MSB first, Master
    // CPOL = 1, CPHA =0
    // SCK Takt = 1/2 XTAL
    SPCR |= _BV(SPE) | _BV(MSTR) | _BV(CPOL);
    SPSR |= _BV(SPI2X);    // double speed aktivieren
    
    SPDR = 0xff;            // Dummy Daten, um SPIF zu setzen
    
    // Übertragungen beendet?
    while (!(SPSR & _BV(SPIF)));
}

uint8_t DipSwitch::getValue()
{    
    // Parallele Daten (Byte) ==> shift register
    // 74xx165 PL => LOW => HIGH
    // CLK ist im Ruhezustand schon auf HIGH, CPOL=1
    PORTB &= ~_BV(PIN_SPI_SS);
    PORTB |=  _BV(PIN_SPI_SS);

    // Übertragungen beendet?
    while (!(SPSR & _BV(SPIF)));
    
    // Dummy Daten, um Übertragung zu starten
    SPDR = 0xff;

    // Übertragungen beendet?
    while (!(SPSR & _BV(SPIF)));

    return SPDR;
}