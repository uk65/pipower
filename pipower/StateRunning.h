#ifndef __STATE_RUNNING_H__
#define __STATE_RUNNING_H__

#include "State.h"

class StateRunning : public State
{
public:

    virtual void entryAction();
};

#endif // __STATE_RUNNING_H__
