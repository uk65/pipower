#ifndef __STATE_STARTING_H__
#define __STATE_STARTING_H__

#include "State.h"

class StateStarting : public State
{
public:

    virtual void entryAction();
    virtual void tickAction();
};

#endif // __STATE_STARTING_H__
