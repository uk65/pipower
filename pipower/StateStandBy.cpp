/**
 * \class StateStandBy
 * \brief StateStandBy repräsentiert den Ruhestatus der State-Maschine.
 *
 * In diesem Zustand ist der Microcontroller im Schlafmodus und der
 * Raspberry Pi ist ausgeschaltet und stromlos. Die Status-LED ist aus.
 * Verlassen wird dieser Zustand durch einen Druck auf den zentralen
 * Tastschalter.
 */

#include "StateStandBy.h"
#include "Hardware.h"

/**
 * \brief Hier werden die Stromversorgung des Raspberry Pi und die Status-LED
 *        abgeschaltet.
 */
void StateStandBy::entryAction()
{
    LED::setMode(LED::LedMode::Off);
    Power::setPower(false);
}
