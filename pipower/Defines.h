#ifndef __DEFINES_H__
#define __DEFINES_H__

#include <avr/io.h>

/*
|--------------------------------------------------------------------
| 328p       Uno       | 328p       Uno | 328p       Uno            |
|--------------------------------------------------------------------
| PINB0 <==> D8        | PINC0 <==> A0  | PIND0 <==> D0             |
| PINB1 <==> D9        | PINC1 <==> A1  | PIND1 <==> D1             |
| PINB2 <==> D10       | PINC2 <==> A2  | PIND2 <==> D2 (INT0)      |
| PINB3 <==> D11       | PINC3 <==> A3  | PIND3 <==> D3 (INT1, PWM) |
| PINB4 <==> D12       | PINC4 <==> A4  | PIND4 <==> D4             |
| PINB5 <==> D13 (LED) | PINC5 <==> A5  | PIND5 <==> D5             |
| PINB6                | PINC6          | PIND6 <==> D6             |
| PINB7                |                | PIND7 <==> D7             |
|--------------------------------------------------------------------
| 328p       Nano      | 328p      Nano | 328p       Nano           |
|--------------------------------------------------------------------
| PINB0 <==> D8        | PINC0 <==> A0  | PIND0 <==> RX0            |
| PINB1 <==> D9        | PINC1 <==> A1  | PIND1 <==> TX1            |
| PINB2 <==> D10       | PINC2 <==> A2  | PIND2 <==> D2 (INT0)      |
| PINB3 <==> D11       | PINC3 <==> A3  | PIND3 <==> D3 (INT1, PWM) |
| PINB4 <==> D12       | PINC4 <==> A4  | PIND4 <==> D4             |
| PINB5 <==> D13 (LED) | PINC5 <==> A5  | PIND5 <==> D5             |
| PINB6                | PINC6          | PIND6 <==> D6             |
| PINB7                |                | PIND7 <==> D7             |
|--------------------------------------------------------------------
| 328p       Pro mini  | 328p  Pro mini | 328p       Pro mini       |
|--------------------------------------------------------------------
| PINB0 <==> 8         | PINC0 <==> A0  | PIND0 <==> RX1            |
| PINB1 <==> 9         | PINC1 <==> A1  | PIND1 <==> TX0            |
| PINB2 <==> 10        | PINC2 <==> A2  | PIND2 <==> 2 (INT0)       |
| PINB3 <==> 11 (MOSI) | PINC3 <==> A3  | PIND3 <==> 3 (INT1, PWM)  |
| PINB4 <==> 12 (MISO) | PINC4 <==> A2/3| PIND4 <==> 4              |
| PINB5 <==> 13 (SCK)  | PINC5 <==> A3/ | PIND5 <==> 5              |
| PINB6          LED   | PINC6          | PIND6 <==> 6              |
| PINB7                |                | PIND7 <==> 7              |
|--------------------------------------------------------------------

|-------------------------------------------------------------------|
| Atmel-ICE pins |  SPI pins  | Arduino pro mini                    |
|-------------------------------------------------------------------|
|  0   TCK       |  3  SCK    |     13                              |
|  1   GND       |  6  GND    |     GND  (rechts 2. von oben)       |
|  2   TDO       |  1  MISO   |     12                              |
|  3   VTG       |  2  VTG    |     VCC  (rechts 4. von oben)       |
|  4   TMS       |            |                                     |
|  5   nSRST     |  5  /RESET |     RST  (rechts 3. von oben)       |
|  6   - nc -    |            |                                     |
|  7   nTRST     |            |                                     |
|  8   TDI       |  4  MOSI   |     11                              |
|  9   GND       |            |                                     |
|-------------------------------------------------------------------|
|                                                                   |
|                                      *NASE*              ###      |
|  von hinten (Kabel) gesehen   RESET | SCK  | MISO      5  3  1    |
|                               GND   | MOSI | +5V       6  4  2    |
|                                                                   |
|-------------------------------------------------------------------|
*/

#define PIN_FROM_PI         PIND2   // Pulse vom Pi (Lebenszeichen)
#define PIN_PWM             PINB1   // Signalgenerator
#define PIN_LED             PIND4   // Die zentrale LED
#define PIN_TO_PI           PIND5   // Signale an den PI
#define PIN_POWER           PIND6   // Pi-Power
#define PIN_PUSHBUTTON      PIND7   // Der Taster (Schliesser)

#define PIN_DIL_00000001    PINB0   // DIL-Schalter 0
#define PIN_DIL_00000010    PINB1   // DIL-Schalter 1
#define PIN_DIL_00000100    PINB2   // DIL-Schalter 2
#define PIN_DIL_00001000    PINB3   // DIL-Schalter 3
#define PIN_DIL_00010000    PINB4   // DIL-Schalter 4
#define PIN_DIL_00100000    PINB5   // DIL-Schalter 5
#define PIN_DIL_01000000    PINB6   // DIL-Schalter 6
#define PIN_DIL_10000000    PINB7   // DIL-Schalter 7

#define PIN_SPI_SS          PINB2   // 74xx165  PL
//#define PIN_SPI_MOSI        PINB3
#define PIN_SPI_MISO        PINB4   // 74xx165  DIN
#define PIN_SPI_SCK         PINB5   // 74xx165  CLK

#if BOARD == pro-mini
  #define PIN_PI_PULSE      PINB0   // Pi-Simulation (muss mit PIN_FROM_PI verbunden sein)
#else
  #define PIN_PI_PULSE      PINB4   // Pi-Simulation (muss mit PIN_FROM_PI verbunden sein)
#endif
#define PIN_LED_BOARD       PINB5   // LED auf dem Arduino Uno Board

#define PI_CHECK_INTERVAL_SEC           3   // Zeit in Sekunden, in der jeweils die Pi-Pulse gezählt werden
#define PI_CHECK_INTERVAL_PULSE_COUNT   2   // Anzahl der Pulse, die mindestens vom Pi kommen müssen
#define POWER_ON_DELAY_SEC              5   // Maximale Wartezeit auf Pi-Pulse nach dem Einschalten in Sekunden
#define POWER_OFF_DELAY_SEC             5   // Maximale Wartezeit auf Pi-Pulse vor dem Ausschalten in Sekunden

#endif // __DEFINES_H__
