#ifndef __STATE_STANDBY_H__
#define __STATE_STANDBY_H__

#include "State.h"

class StateStandBy : public State
{
public:

    virtual void entryAction();
};

#endif // __STATE_STANDBY_H__
