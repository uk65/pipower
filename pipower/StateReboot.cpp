
#include "StateReboot.h"
#include "Hardware.h"

void StateReboot::entryAction()
{
    State::entryAction();
    
    LED::setMode(LED::LedMode::Blinking);
    PiSignal::setSignal(2);
}

void StateReboot::tickAction()
{
    State::tickAction();
    
    if (m_ticks > 3)
        PiSimulation::setSimulation(false);
}
