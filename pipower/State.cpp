/**
 * \class State
 * \brief Die Klasse State ist die Basisklassen aller Modus-Klassen der
 *        State-Maschine.
 */

#include "State.h"

/**
 * \brief entryAction() wird bei der Aktivierung eines Status einmalig aufgerufen.
 *        Hier können Initialisierungen durchgeführt werden. Hier in der Basisklasse
 *        wird ein 8-bit Zähler initialisiert, der bei jedem Aufruf der Methode
 *        \ref tickAction() inkrementiert wird.
 */
void State::entryAction()
{
    m_ticks = 0;
}

/**
 * \brief tickAction() wird einmal pro Sekunde für den aktiven Status aufgerufen.
 *        Die Basisklasse inkrementiert den 8-bit Zähler, der in der Methode
 *        \ref entryAction() mit 0 initialisiert wird. Bei Überlauf wird der Zähler
 *        auf 0 zurückgesetzt.
 */
void State::tickAction()
{
    if (++m_ticks == 0xff)
        m_ticks = 0;
}

/**
 * \brief exitAction() wird vor dem Verlassen eines Status einmalig aufgerufen.
 *        Die Implementierung der Basisklasse tut nichts.
 */
void State::exitAction()
{
}
