/**
 * \mainpage pipower Dokumentation
 *
 * \section intro Einführung
 *
 * Der Einplatinencomputer Raspberry Pi eignet sich durch seine GPIO-Pins
 * sehr gut zur Steuerung verschiedener Hardware-Komponenten.
 * Wird er an seine 5V Versorgungsspannung angeschlossen, startet er
 * und lädt das installierte Linux Betriebssystem (meistens Raspbian).
 * Allerdings darf er wie jeder Linux Computer nicht einfach von der
 * Versorgungsspannung getrennt werden, sondern muß sauber heruntergefahren
 * werden. Andernfalls können die Daten auf der als Massenspeicher
 * dienenden SD-Karte beschädigt werden, was schlimmstenfalls eine
 * Neuinstallation des kompletten Systems erfordert.
 *
 * Wünschenswert wäre die Möglichkeit, den Raspberry Pi beziehungsweise
 * das Gerät, das mit ihm realisiert wird, mit einem einfachen Tastschalter
 * ein- und ausschalten zu können, ohne sich um die Linux-Interna kümmern
 * zu müssen.
 *
 * Außerdem sollten auch vom Raspberry Pi angestoßene Halt- und
 * Reboot-Vorgänge erkannt werden und entsprechend darauf reagiert werden.
 *
 * \section features Anforderungen
 *
 * Damit ergeben sich die folgenden Anforderungen:
 *
 * - Ein- und Ausschalten des Raspberry Pi mit einem einzigen Tastschalter.
 * - Visualisierung des Betriebszustandes des Raspberry Pi durch eine
 *   oder mehrere LEDs.
 * - Erkennen von softwareseitigen, vom Raspberry Pi ausgelösten
 *   Reboot- oder Halt-Aktionen.
 * - Trennen des Raspberry Pi von der Stromversorgung nach dem sicheren
 *   Herunterfahren, unabhängig davon, ob der Raspberry Pi hard- oder
 *   softwareseitig heruntergefahren wurde.
 * - Um den Raspberry Pi auch von außen neu starten zu können, soll es
 *   eine Reboot Möglichkeit per Hardware, also mit dem auch zum Ein-
 *   und Ausschalten verwendeten Tastschalter geben. Ein Neustart soll
 *   aus dem Betrieb durch einen langen Tastendruck ausgelöst werden.
 *
 * Da das saubere Herunterfahren des Raspberry Pi ein eigentlich bei jedem
 * Projekt auftretendes Problem darstellt, existieren dafür auf dem Markt
 * einige Lösungen, die meistens auf Timerschaltungen basieren.
 *
 * Halt oder Reboot des Raspberry Pi können ausschließlich von diesem
 * ausgelöst werden. Dazu sind Root-Rechte nötig. Es existiert keine
 * Möglichkeit eines Hardware-Halts/Reboots.
 *
 * Um eine maximale Flexibilität bei gleichzeitig hoher Sicherheit zu
 * erreichen, wird hier ein Lösungsansatz mit einem Microcontroller als
 * steuerndes Element gewählt.
 *
 * \section design Aufbau
 *
 * Das Projekt pipower besteht aus zwei Komponenten:
 *
 * - der auf einem Microcontroller basierenden Steuereinheit.
 * - einem auf dem Raspberry Pi laufenden Programm, das mit der
 *   Steuereinheit kommuniziert.
 *
 * Die Steuereinheit besteht aus einem Atmel AVR Microcontroller ATmega328p
 * mit entsprechender Peripherie. Dieser Microcontroller kommt auch in den
 * Arduino Boards Uno, Nano und Pro mini zum Einsatz. Diese sind im Internet
 * als billige Klone erhältlich. Damit bietet sich die Verwendung eines solchen
 * Klons an. Den Pro mini gibt es außerdem als 3,3V Variante, was eine
 * Pegelwandelung überflüssig macht (der Raspberry Pi arbeitet mit 3,3V Ein- und
 * Ausgängen und ist nicht 5V-tolerant). Allerdings hat er keine USB-Schnittstelle,
 * was den Einsatz zusätzlicher Hardware erfordert. Da die Arduino IDE nicht
 * verwendet wird und auch der Arduino Bootloader nicht benötigt wird,
 * spielt das keine Rolle. Der Programmcode für den Microcontroller wird direkt
 * mit dem entsprechenden AVR Toolchain (GCC Compiler, AVR Libc) erstellt und der
 * Microcontroller mit einem Atmel-ICE programmiert. Die 3,3V Variante des
 * Arduino Pro mini läuft im Gegensatz zur 5V Version nur mit 8 MHz statt 16 MHz,
 * was aber in diesem Fall unkritisch ist.
 *
 * Das Programm auf dem Raspberry Pi ist als Daemon realisiert, der beim
 * Hochfahren gestartet wird. Es sendet regelmäßig Impulse an die Steuereinheit.
 * Bleiben diese Lebenszeichen aus, muß die Steuereinheit reagieren.
 * Außerdem reagiert das Programm auf Befehle der Steuereinheit.
 * Diese werden gesendet, wenn der Raspberry Pi entweder ausgeschaltet oder
 * neu gestartet werden soll (kurzer oder langer Tastendruck).
 *
 * \section flow Abläufe
 *
 * Im ausgeschalteten Zustand des Geräts befindet sich der Microcontroller im
 * extrem sparsamen Schlafzustand und der Raspberry Pi ist stromlos.
 * Wird das Gerät durch einen Druck auf den Tastschalter eingeschaltet,
 * geht der Microcontroller in den normalen Betriebszustand über und schaltet
 * die Stromversorgung des Raspberry Pi ein. Dieser startet daraufhin.
 * Der Microcontroller wartet eine definierte Zeit auf Lebenszeichen des
 * Raspberry Pi von dessen Daemon. Treffen diese nicht ein, ist der Start
 * des Raspberry Pi fehlgeschlagen und die Stromversorgung wird abgeschaltet.
 *
 * Ist der Raspberry Pi erfolgreich gestartet, wartet der Micorcontroller in
 * regelmäßigen Abständen auf dessen Lebenszeichen. Treffen diese nicht mehr ein,
 * ist der Raspberry Pi entweder durch einen Fehler verstorben oder es wurde
 * eine softwareseitiger Reboot / Herunterfahren veranlaßt. In diesem Fall wird
 * noch eine festgelegte Zeitspanne auf neue Lebenszeichen vom Raspberry Pi gewartet.
 * Treffen diese wieder ein, wurde der Raspberry Pi neu gestartet. Sind nach
 * Ablauf der Wartezeit keine Lebenszeichen vom Raspberry Pi eingegangen,
 * wird dessen Stromzufuhr unterbrochen und der Microcontroller geht in den
 * Schlafmodus.
 *
 * Durch kurzen oder langen Tastdruck im normalen Betriebszustand werden Kommandos
 * vom Microcontroller an den Daemon im Raspberry Pi gesendet. Der Daemon
 * wertet diese Kommandos aus und leitet die entsprechenden Aktionen ein (Halt oder
 * Reboot). Als Folge unterbleiben die Lebenszeichen des Raspberry Pi an den 
 * Microcontroller, der nach der schon oben besprochenen Wartezeit die Stromzufuhr
 * des Raspberry Pi unterbricht.
 *
 * \section modi Betriebsmodi
 *
 * pipower kannn sich in genau einem der folgenden Betriebszustände befinden:
 *
 * -# Stand by
 *    Dies ist bezogen auf pipower der Ruhezustand vor dem Einschalten beziehungsweise
 *    nach dem Herunterfahren des Raspberry Pi. Die Spannungsversorgung ist ausgeschaltet
 *    und die LED ist aus. Dieser Modus kann durch einen kurzen Tastendruck verlassen werden,
 *    der die Spannungsversorgung des Pi einschaltet und dessen Bootvorgang startet.
 *
 * -# Starting
 *    Es wird eine definierte Zeit auf Lebenszeichen des Raspberry Pi gewartet.
 *    Die LED blinkt. Treffen Lebenszeichen vom Raspberry Pi ein, wird der Modus Starting
 *    in Richtung Modus Running verlassen.
 *
 * -# Running
 *    Dies ist der normale Betriebsmodus des Raspberry Pi. Die LED leuchtet konstant.
 *    Ein kurzer Tastendruck löst das Herunterfahren (Modus Shutdown) und ein langer
 *    Tastendruck einen Neustart (Modus Reboot) des Raspberry Pi aus.
 *    Bleiben die Lebenszeichen ohne entsprechenden Tastendruck aus (softwareseitiger
 *    Halt oder Reboot), wird in den Modus Last Chance gegangen.
 *
 * -# Shut down
 *    Es werden die entsprechenden Steuerkommandos an den Daemon des Raspberry Pi
 *    geschickt, die das Herunterfahren einleiten.
 *    Die LED blinkt. Nach dem Ausbleiben der Pi Lebenszeichen wird der Modus
 *    in Richtung Last Chance verlassen.
 *
 * -# Reboot
 *    Es werden die entsprechenden Steuerkommandos an den Daemon des Raspberry Pi
 *    geschickt, die den Neustart einleiten.
 *    Die LED blinkt. Nach dem Ausbleiben der Pi Lebenszeichen wird der Modus
 *    in Richtung Last Chance verlassen.
 *
 * -# Last Chance
 *    Hier wird eine definierte Zeit auf Lebenszeichen vom Raspberry Pi gewartet.
 *    Im Fall eines Neustarts hat der Raspberry Pi Zeit zu booten.
 *    Die LED blitzt in kurzen Abständen.
 *    Treffen Lebenszeichen ein, wird in den Modus Running gegangen, bleiben diese aus,
 *    ist der nächste Modus Stand by.
 */

#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#include "Defines.h"
#include "PiPower.h"
#include "Hardware.h"
#include "SquarePulse.h"

#define COUNTER_TOP_16(prescale,ms)     (uint16_t)(((F_CPU / (prescale)) * (0.001 * (ms))) + 1.0)
#define COUNTER_TOP_8(prescale,ms)      (uint8_t)COUNTER_TOP_16((prescale),(ms))


uint8_t g_piPulses = 0;
bool    g_piOK     = 0;

//=============================================================================
// Key DeBouncing
//=============================================================================

#define REPEAT_MASK              _BV(PIN_PUSHBUTTON)
#define REPEAT_START             100    // after 1000ms
#define REPEAT_NEXT               20    // every 200ms

volatile uint8_t  g_keyState    = 0;
volatile uint8_t  g_keyPress    = 0;
volatile uint8_t  g_keyRepeat   = 0;

//-----------------------------------------------------------------------------
// check if a key has been pressed. Each pressed key is reported only once.
//-----------------------------------------------------------------------------
uint8_t getKeyPress(uint8_t keyMask)
{
    cli();                      // read and clear atomic !
    keyMask &= g_keyPress;       // read key(s)
    g_keyPress ^= keyMask;       // clear key(s)
    sei();
    return keyMask;
}
 
//-----------------------------------------------------------------------------
// check if a key has been pressed long enough such that the
// key repeat functionality kicks in. After a small setup delay
// the key is reported being pressed in subsequent calls
// to this function. This simulates the user repeatedly
// pressing and releasing the key.
//-----------------------------------------------------------------------------
uint8_t getKeyRepeat(uint8_t keyMask)
{
    cli();                      // read and clear atomic !
    keyMask &= g_keyRepeat;     // read key(s)
    g_keyRepeat ^= keyMask;     // clear key(s)
    sei();
    return keyMask;
}

//-----------------------------------------------------------------------------
// check if a key is pressed right now
//-----------------------------------------------------------------------------
uint8_t getKeyState(uint8_t keyMask)
{
    keyMask &= g_keyState;
    return keyMask;
}

//-----------------------------------------------------------------------------
// short key press
//-----------------------------------------------------------------------------
uint8_t getKeyShort(uint8_t keyMask)
{
    cli();            // read key state and key press atomic !
    return getKeyPress(~g_keyState & keyMask);
}
 
//-----------------------------------------------------------------------------
// long key press
//-----------------------------------------------------------------------------
uint8_t getKeyLong(uint8_t keyMask)
{
    return getKeyPress(getKeyRepeat(keyMask));
}

//=============================================================================
// Key DeBouncing Ende
//=============================================================================

ISR(TIMER0_COMPA_vect)
{
    //-------------------------------------------------------------------------
    // Key-DeBouncing
    //-------------------------------------------------------------------------
    static uint8_t ct0 = 0xff;
    static uint8_t ct1 = 0xff;
    static uint8_t rpt;
 
//  uint8_t i = g_keyState ^ ~PIND;         // key changed (active low) ?
    uint8_t i = g_keyState ^ PIND;          // key changed (active high) ?

    ct0 = ~(ct0 & i);                       // reset or count ct0
    ct1 = ct0 ^ (ct1 & i);                  // reset or count ct1
    i &= ct0 & ct1;                         // count until roll over ?
    g_keyState ^= i;                        // then toggle debounced state
    g_keyPress |= g_keyState & i;           // 0->1: key press detect
 
    if ((g_keyState & REPEAT_MASK) == 0)    // check repeat function
        rpt = REPEAT_START;                 // start delay
    if (--rpt == 0)
    {
        rpt = REPEAT_NEXT;                  // repeat delay
        g_keyRepeat |= g_keyState & REPEAT_MASK;
    }
    //-------------------------------------------------------------------------
    
    static uint8_t tick10ms   = 0;
    static uint8_t tick100ms  = 0;
    static uint8_t tick1000ms = 0;
    static uint8_t piPulses   = 0;
    
    if (++tick10ms == 10)
    {
        tick10ms = 0;

        LED::tick100ms(tick100ms);
        PiSignal::tick100ms(tick100ms);
        PiSimulation::tick100ms(tick100ms);

        if (++tick100ms == 10)
        {
            tick100ms = 0;

            // 1-Sekunden Tick
            PiPower::sendEvent(PiPower::Event::Tick);

            // Pi-Lebenszeichen
            if (++tick1000ms == PI_CHECK_INTERVAL_SEC)
            {
                const uint8_t diffPulses = (g_piPulses >= piPulses)
                    ? (g_piPulses - piPulses)
                    : (0xff - piPulses + g_piPulses);
                g_piOK = (diffPulses > PI_CHECK_INTERVAL_PULSE_COUNT);
                piPulses = g_piPulses;
                tick1000ms = 0;
            }
        }
    }
}

ISR(INT0_vect)
{
    // Überlauf Pulsezähler abfangen 
    if (g_piPulses == 0xff)
        g_piPulses = 0;    
    g_piPulses++;
    
//    PORTB ^= _BV(PIN_LED_BOARD);
}

int main()
{
    //-------------------------------------------------------------------------
    // Output Pins
    //-------------------------------------------------------------------------
    DDRB  |= _BV(PIN_PWM) | _BV(PIN_PI_PULSE) | _BV(PIN_LED_BOARD);
    DDRD  |= _BV(PIN_LED) | _BV(PIN_TO_PI) | _BV(PIN_POWER);
    // default low <==> active high
    PORTB &= ~(_BV(PIN_PWM) | _BV(PIN_PI_PULSE) | _BV(PIN_LED_BOARD));
    PORTD &= ~(_BV(PIN_LED) | _BV(PIN_POWER));
    // default high <==> active low
    PORTD |= _BV(PIN_TO_PI);

    //-------------------------------------------------------------------------
    // Input Pins
    //-------------------------------------------------------------------------
    DDRD &= ~_BV(PIN_FROM_PI);

    DDRB &= ~(_BV(PIN_DIL_00000001) | _BV(PIN_DIL_00000010));
    DDRB &= ~(_BV(PIN_DIL_00000100) | _BV(PIN_DIL_00001000));
    DDRB &= ~(_BV(PIN_DIL_00010000) | _BV(PIN_DIL_00100000));
    DDRB &= ~(_BV(PIN_DIL_01000000) | _BV(PIN_DIL_10000000));

    // Pull up Widerstände an
    PORTB |= _BV(PIN_DIL_00000001) | _BV(PIN_DIL_00000010);
    PORTB |= _BV(PIN_DIL_00000100) | _BV(PIN_DIL_00001000);
    PORTB |= _BV(PIN_DIL_00010000) | _BV(PIN_DIL_00100000);
    PORTB |= _BV(PIN_DIL_01000000) | _BV(PIN_DIL_10000000);
    
    SquarePulse squarePulse;

    // Timer 0: 100Hz
    TCCR0A |= _BV(WGM01);
    OCR0A  |=  COUNTER_TOP_8(1024,10);
    TIMSK0 |= _BV(OCIE0A);
    TCCR0B |= _BV(CS02) | _BV(CS00);
    
    // Externer Interrupt 0, steigende Flanke: hier kommen die Pi-Pulse an.
    EICRA |= _BV(ISC01) | _BV(ISC00);
    EIMSK |= _BV(INT0);

    PiPower::init();

    sei();

    bool prevPiOK = g_piOK;

    while (true)
    {
        if (getKeyShort(_BV(PIN_PUSHBUTTON)))
            PiPower::sendEvent(PiPower::Event::KeyPressShort);
        if (getKeyLong(_BV(PIN_PUSHBUTTON)))
            PiPower::sendEvent(PiPower::Event::KeyPressLong);
        
        if (g_piOK != prevPiOK)
        {
            PiPower::sendEvent(g_piOK ? PiPower::Event::PiPulse : PiPower::Event::NoPiPulse);
            prevPiOK = g_piOK;
        }
        
        PiPower::process();
        
        squarePulse.setGenerator();
    }

    return 0;
}

