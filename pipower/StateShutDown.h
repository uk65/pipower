#ifndef __STATE_SHUTDOWN_H__
#define __STATE_SHUTDOWN_H__

#include "State.h"

class StateShutDown : public State
{
public:

    virtual void entryAction();
    virtual void tickAction();
};

#endif // __STATE_SHUTDOWN_H__
