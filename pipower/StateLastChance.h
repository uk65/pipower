#ifndef __STATE_LASTCHANCE_H__
#define __STATE_LASTCHANCE_H__

#include "State.h"

class StateLastChance : public State
{
public:

    virtual void entryAction();
};

#endif // __STATE_LASTCHANCE_H__
