#ifndef __STATE_H__
#define __STATE_H__

#include <stdint.h>

class State
{
protected:
    
    uint8_t   m_ticks;
    
public:
    
    virtual void entryAction();
    virtual void tickAction();
    virtual void exitAction();
};

#endif // __STATE_H__
