#ifndef __HARDWARE_H__
#define __HARDWARE_H__

#include <stdint.h>

//-----------------------------------------------------------------------------

namespace LED
{
    enum LedMode
    {
        Off,
        Flashing,
        Blinking,
        On,
    };
    
    void tick100ms(const uint8_t tick);
    void setMode(const LedMode ledMode);
};

//-----------------------------------------------------------------------------

namespace DebugLED
{
    void flash(const uint8_t count);
};

//-----------------------------------------------------------------------------

namespace Power
{
    void setPower(const bool powerOn);
};

//-----------------------------------------------------------------------------

namespace PiSignal
{
    void tick100ms(const uint8_t tick);
    void setSignal(const uint8_t piSignals);
};

//-----------------------------------------------------------------------------

namespace PiSimulation
{
    void tick100ms(const uint8_t tick);
    void setSimulation(const bool piSimulation);
};

//-----------------------------------------------------------------------------

namespace PWM
{
};

//-----------------------------------------------------------------------------

namespace DipSwitch
{
    void init();
    uint8_t getValue();
};

//-----------------------------------------------------------------------------

#endif // __HARDWARE_H__
