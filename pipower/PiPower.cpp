/**
 * \brief Die pipower Steuereinheit ist als sogenannter endlicher Automat
 *        (state machine) implementiert. Jeder Zustand, in dem sich die
 *        Steuereinheit befinden kann, wird durch einen eindeutigen Status
 *        repräsentiert. Wechsel zwischen den Stati werden durch Übergänge
 *        (transitions) definiert. Ausgelöst werden die Übergänge zum Beispiel
 *        durch Hardware-Ereignisse (Schalter, Timeouts).
 */

#include "PiPower.h"
#include "StateStandBy.h"
#include "StateStarting.h"
#include "StateRunning.h"
#include "StateShutDown.h"
#include "StateReboot.h"
#include "StateLastChance.h"

using namespace PiPower;

static StateStandBy     stateStandBy;
static StateStarting    stateStarting;
static StateRunning     stateRunning;
static StateShutDown    stateShutDown;
static StateReboot      stateReboot;
static StateLastChance  stateLastChance;

State     *initState    = &stateStandBy;
State     *currentState = initState;
uint16_t   timeInState  = 0;
int        evQueueSize  = EventQueueSize;;
int        evQueueHead  = 0;
int        evQueueTail  = 0;
Event      evQueue[EventQueueSize];


struct Transition
{
    State     *currentState;
    Event      event;
    uint16_t   maxTimeBeforeTransition;
    State     *nextState;
} transitions[] =
{
    { &stateStandBy,     Event::KeyPressShort,  0,            &stateStarting   },

    { &stateStarting,    Event::PiPulse,        0,            &stateRunning    },
    { &stateStarting,    Event::Tick,           Seconds(10),  &stateStandBy    },

    { &stateRunning,     Event::KeyPressShort,  0,            &stateShutDown   },
    { &stateRunning,     Event::KeyPressLong,   0,            &stateReboot     },
    { &stateRunning,     Event::NoPiPulse,      0,            &stateLastChance },
    
    { &stateShutDown,    Event::NoPiPulse,      0,            &stateLastChance },
    { &stateShutDown,    Event::Tick,           Seconds(5),   &stateLastChance },
    
    { &stateReboot,      Event::NoPiPulse,      0,            &stateLastChance },
    { &stateReboot,      Event::Tick,           Seconds(5),   &stateLastChance },

    { &stateLastChance,  Event::PiPulse,        0,            &stateRunning    },
    { &stateLastChance,  Event::Tick,           Seconds(10),  &stateStandBy    },
};

void PiPower::init()
{
    currentState->entryAction();
}

void PiPower::sendEvent(const Event e)
{
    if (!evQueueIsFull())
    {
        evQueue[evQueueTail] = e;
        evQueueTail = (evQueueTail + 1) & (evQueueSize - 1);
    }
}

void PiPower::process()
{
    if (evQueueIsEmpty())
        return;
    
    Event e = evQueue[evQueueHead];
    evQueueHead = (evQueueHead + 1) & (evQueueSize - 1);

    for (uint8_t i = 0; i < sizeof(transitions) / sizeof(Transition); i++)
    {
        Transition &t = transitions[i];
        
        if (((currentState == t.currentState) && (e == t.event)) || (t.event == Event::NoEvent))
        {
            if (e == Event::Tick)
            {
                // System Tick
                t.currentState->tickAction();
                timeInState++;
                if ((t.maxTimeBeforeTransition != 0) && (timeInState < t.maxTimeBeforeTransition))
                {
                    // Normal Tick
                    t.currentState->tickAction();
                }
                else
                {
                    // Force Transition because of an Timeout
                    t.currentState->exitAction();
                    currentState = t.nextState;
                    currentState->entryAction();
                    timeInState = 0;
                }
            }
            else
            {
                timeInState = 0;
                t.currentState->exitAction();
                currentState = t.nextState;
                currentState->entryAction();
            }
            break;
        }
    }
}

bool PiPower::evQueueIsEmpty()
{
    return (evQueueHead == evQueueTail);
}

bool PiPower::evQueueIsFull()
{
    return (((evQueueTail + 1) & (evQueueSize - 1)) == evQueueHead);
}

