
#include "StateShutDown.h"
#include "Hardware.h"

void StateShutDown::entryAction()
{
    State::entryAction();
    
    LED::setMode(LED::LedMode::Blinking);
    PiSignal::setSignal(1);
}

void StateShutDown::tickAction()
{
    State::tickAction();

    if (m_ticks > 3)
        PiSimulation::setSimulation(false);
}
