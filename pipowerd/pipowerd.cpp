
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <syslog.h>
#include <signal.h>
#include <getopt.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <stdarg.h>
#include <time.h>

extern "C" {
#include <libconfig.h>    
}

#include <wiringPi.h>

static bool  g_running          = false;
static char *g_configFileName   = nullptr;
static char *g_pidFileName      = nullptr;
static char *g_logFileName      = nullptr;
static int   g_pidFd            = -1;
static char *g_appName          = nullptr;
static FILE *g_logStream        = nullptr;
static bool  g_startDaemonized  = false;
static int   g_pulseCount       = 0;

//-------------------------------------
//        Physical    BCM     WiringPi
//-------------------------------------
// Input     15      22          3
// Output    16      23          4
//-------------------------------------
static int   g_inputPin         = 3;
static int   g_outputPin        = 4;
static int   g_pulseLength      = 200;
static int   g_pulseGap         = 800;
static int   g_shutDownPulses   = 1;
static int   g_rebootPulses     = 2;

/**
 * \brief Read configuration from config file
 */
void readConfigFile(const bool reload)
{
    if (g_configFileName != nullptr)
    {
        config_t cfg;
        config_init(&cfg);

        if (config_read_file(&cfg, g_configFileName) == CONFIG_TRUE)
        {
            int ival;
            if (config_lookup_bool(&cfg, "daemon", &ival) == CONFIG_TRUE)
                g_startDaemonized = (bool) ival;

            const char *cval;
            if (config_lookup_string(&cfg, "pidfile", &cval) == CONFIG_TRUE)
                g_pidFileName = const_cast<char *>(cval);

            if (config_lookup_string(&cfg, "logfile", &cval) == CONFIG_TRUE)
                g_logFileName = const_cast<char *>(cval);

            if (config_lookup_int(&cfg, "inputpin", &ival) == CONFIG_TRUE)
                g_inputPin = ival;

            if (config_lookup_int(&cfg, "outputpin", &ival) == CONFIG_TRUE)
                g_outputPin = ival;

            if (config_lookup_int(&cfg, "pulselength", &ival) == CONFIG_TRUE)
                g_pulseLength = ival;

            if (config_lookup_int(&cfg, "pulsegap", &ival) == CONFIG_TRUE)
                g_pulseGap = ival;

            if (config_lookup_int(&cfg, "shutdownpulses", &ival) == CONFIG_TRUE)
                g_shutDownPulses = ival;

            if (config_lookup_int(&cfg, "rebootpulses", &ival) == CONFIG_TRUE)
                g_rebootPulses = ival;

            if (reload)
                syslog(LOG_INFO, "Reloaded configuration file %s of %s", g_configFileName, g_appName);
            else
                syslog(LOG_INFO, "Configuration of %s read from file %s", g_appName, g_configFileName);
        }
        else
        {
            syslog(LOG_ERR, "Error parsing config file %s, line %d: %s", 
                    config_error_file(&cfg), config_error_line(&cfg), config_error_text(&cfg));
        }

        config_destroy(&cfg);
    }
}

/**
 * \brief This function tries to test config file
 */
int testConfigFile(const char *configFileName)
{
    FILE *configFile = fopen(configFileName, "r");
    if (configFile == nullptr)
    {
        fprintf(stderr, "Can't read config file %s\n", configFileName);
        return EXIT_FAILURE;
    }

    config_t cfg;
    config_init(&cfg);
    const int ret = config_read(&cfg, configFile);
    config_destroy(&cfg);

    fclose(configFile);

    if (ret == CONFIG_FALSE)
    {
        fprintf(stderr, "Error parsing config file %s, line %d: %s\n",
                    config_error_file(&cfg), config_error_line(&cfg), config_error_text(&cfg));
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

void openLogFile(const char *logFileName)
{
    if (logFileName != nullptr)
    {
        g_logStream = fopen(logFileName, "a+");
        if (g_logStream == nullptr)
        {
            syslog(LOG_ERR, "Can not open log file: %s, error: %s", logFileName, strerror(errno));
            g_logStream = stdout;
        }
    } else {
        g_logStream = stdout;
    }
}

void logError(const int priority, const char *format, ...)
{
    va_list args;
    va_start(args, format);
    if (g_startDaemonized)
        vsyslog(priority, format, args);
    else
        vfprintf(stderr, format, args);
    va_end(args);
}

/**
 * \brief Callback function for handling signals.
 * \param	sig	identifier of signal
 */
void handleSignal(const int sig)
{
    if (sig == SIGINT)
    {
        fprintf(g_logStream, "Debug: stopping daemon ...\n");
        // Unlock and close lockfile
        if (g_pidFd != -1)
        {
            lockf(g_pidFd, F_ULOCK, 0);
            close(g_pidFd);
        }
        // Try to delete lockfile
        if (g_pidFileName != nullptr)
            unlink(g_pidFileName);
        g_running = false;
        // Reset signal handling to default behavior
        signal(SIGINT, SIG_DFL);
    }
    else if (sig == SIGHUP)
    {
        fprintf(g_logStream, "Debug: reloading daemon config file ...\n");
        readConfigFile(true);
    }
    else if (sig == SIGCHLD)
    {
        fprintf(g_logStream, "Debug: received SIGCHLD signal\n");
    }
}

/**
 * \brief This function will daemonize this app
 */
static void daemonize()
{
    // Fork off the parent process
    pid_t pid = fork();
    if (pid < 0)
        exit(EXIT_FAILURE);

    // Success: Let the parent terminate
    if (pid > 0)
        exit(EXIT_SUCCESS);

    // On success: The child process becomes session leader
    if (setsid() < 0)
        exit(EXIT_FAILURE);

    // Ignore signal sent from child to parent process
    signal(SIGCHLD, SIG_IGN);

    // Fork off for the second time
    pid = fork();
    if (pid < 0)
        exit(EXIT_FAILURE);

    // Success: Let the parent terminate
    if (pid > 0)
        exit(EXIT_SUCCESS);

    // Set new file permissions
    umask(0);

    // Change the working directory to the root directory or another appropriated directory
    chdir("/");

    // Close all open file descriptors
    for (int fd = sysconf(_SC_OPEN_MAX); fd > 0; fd--)
        close(fd);

    // Reopen stdin (fd = 0), stdout (fd = 1), stderr (fd = 2)
    stdin  = fopen("/dev/null", "r");
    stdout = fopen("/dev/null", "w+");
    stderr = fopen("/dev/null", "w+");

    // Try to write PID of daemon to lockfile
    if (g_pidFileName != nullptr)
    {
        g_pidFd = open(g_pidFileName, O_RDWR | O_CREAT, 0640);
        if (g_pidFd < 0)
            exit(EXIT_FAILURE);
        if (lockf(g_pidFd, F_TLOCK, 0) < 0)
            exit(EXIT_FAILURE);
        char str[256];
        sprintf(str, "%d\n", getpid());
        write(g_pidFd, str, strlen(str));
    }
}

/**
 * \brief Print help for this application
 */
void print_help(void)
{
    printf("\n Usage: %s [OPTIONS]\n\n", g_appName);
    printf("  Options:\n");
    printf("   -h --help                  Print this help\n");
    printf("   -c --configfile filename   Read configuration from the file\n");
    printf("   -t --testconfig filename   Test configuration file\n");
    printf("   -l --logfile filename      Write logs to the file\n");
    printf("   -d --daemon                Daemonize this application\n");
    printf("   -p --pidfile filename      PID file used by daemonized app\n");
    printf("\n");
}

void pinHighInterrupt(void)
{
//    time((time_t *) &g_timePinHigh);
}

void pinLowInterrupt(void)
{
    static time_t prevTime = 0;

    time_t timeNow;
    time(&timeNow);

    const double diff = difftime(timeNow, prevTime);
    if (diff > 5.0)
        g_pulseCount = 0;
    if ((diff > 0.09) && (diff < 0.11))
    {
        g_pulseCount++;
        prevTime = timeNow;
    }
}

int main(int argc, char **argv)
{
    g_appName = argv[0];

    static struct option long_options[] =
    {
        { "configfile", required_argument, 0, 'c' },
        { "testconfig", required_argument, 0, 't' },
        { "logfile",    required_argument, 0, 'l' },
        { "help",       no_argument,       0, 'h' },
        { "daemon",     no_argument,       0, 'd' },
        { "pidfile",    required_argument, 0, 'p' },
        {  nullptr,     0,                 0,  0  }
    };

    int value, option_index = 0;

    while ((value = getopt_long(argc, argv, "c:l:t:p:dh", long_options, &option_index)) != -1)
    {
        switch (value)
        {
            case 'c':
                g_configFileName = strdup(optarg);
                break;
            case 'l':
                g_logFileName = strdup(optarg);
                break;
            case 'p':
                g_pidFileName = strdup(optarg);
                break;
            case 't':
                return testConfigFile(optarg);
            case 'd':
                g_startDaemonized = true;
                break;
            case 'h':
                print_help();
                return EXIT_SUCCESS;
            case '?':
                print_help();
                return EXIT_FAILURE;
            default:
                break;
        }
    }

    if (g_startDaemonized)
        daemonize();

    openlog(argv[0], LOG_PID | LOG_CONS, LOG_DAEMON);
    syslog(LOG_INFO, "Started %s", g_appName);

    // Daemon will handle two signals
    signal(SIGINT, handleSignal);
    signal(SIGHUP, handleSignal);

    openLogFile(g_logFileName);
    readConfigFile(false);

    if (wiringPiSetup() < 0)
    {
        logError(LOG_ERR, "Unable to setup wiringPi: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    if (wiringPiISR(g_inputPin, INT_EDGE_RISING, &pinHighInterrupt) < 0)
    {
        logError(LOG_ERR, "Unable to setup ISR: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    if (wiringPiISR(g_inputPin, INT_EDGE_FALLING, &pinLowInterrupt) < 0)
    {
        logError(LOG_ERR, "Unable to setup ISR: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    pinMode(g_outputPin, OUTPUT);

    // This global variable can be changed in function handling signal
    g_running = true;

    while (g_running)
    {
        digitalWrite(g_outputPin, HIGH);
        delay(g_pulseLength);
        digitalWrite(g_outputPin, LOW);
        delay(g_pulseGap);

        if (g_pulseCount == g_shutDownPulses)
            system("sudo halt");
        if (g_pulseCount == g_rebootPulses)
            system("sudo reboot");
    }

    if (g_logStream != stdout)
        fclose(g_logStream);

    syslog(LOG_INFO, "Stopped %s", g_appName);
    closelog();

    if (g_configFileName != nullptr)
        free(g_configFileName);
	if (g_logFileName != nullptr)
        free(g_logFileName);
	if (g_pidFileName != nullptr)
        free(g_pidFileName);

	return EXIT_SUCCESS;
}
