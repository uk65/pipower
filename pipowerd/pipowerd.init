#!/bin/sh
#
#       /etc/rc.d/init.d/pipowerd
#
#       init script for pipowerd
#
# chkconfig:   2345 20 80
# description: pipowerd daemon

### BEGIN INIT INFO
# Provides:       pipowerd
# Required-Start: $rsyslog
# Required-Stop:
# Should-Start:
# Should-Stop:
# Default-Start: 2 3 4 5
# Default-Stop:  0 1 6
# Short-Description: start and stop of pipowerd daemon
# Description: pipowerd daemon
### END INIT INFO

. /etc/rc.d/init.d/functions

prog="pipowerd"
app="/usr/bin/$prog"
lockfile="/var/lock/subsys/$prog"
logfile="/var/log/$prog.log"
configfile="/etc/$prog.conf"
proguser=root

[ -e /etc/sysconfig/$prog ] && . /etc/sysconfig/$prog

start() {
    [ -x $exec ] || exit 5

    echo -n $"Starting $prog: "
    pipowerd --user $proguser $app --configfile $configfile --logfile $logfile --daemon
    RETVAL=$?
    [ $RETVAL -eq 0 ] && touch $lockfile
    echo
    return $RETVAL
}

stop() {
    echo -n $"Stopping $prog: "
    killproc $prog -INT
    RETVAL=$?
    echo
    [ $RETVAL -eq 0 ] && rm -f $lockfile
    return $RETVAL
}

configtest() {
    $app --testconfig $configfile
    RETVAL=$?
    [ $RETVAL -eq 0 ] && echo -n "Syntax OK"
    echo
    return $RETVAL
}

restart() {
    stop
    start
}

reload() {
    echo -n $"Reloading $prog: "
    killproc $prog -HUP
    RETVAL=$?
    echo
    return $RETVAL
}

force_reload() {
    restart
}

rh_status() {
    status $prog
}

rh_status_q() {
    rh_status >/dev/null 2>&1
}

case "$1" in
    start)
        rh_status_q && exit 0
        $1
        ;;
    stop)
        rh_status_q || exit 0
        $1
        ;;
    restart)
        $1
        ;;
    reload)
        rh_status_q || exit 7
        $1
        ;;
    force-reload)
        force_reload
        ;;
    status)
        rh_status
        ;;
    configtest)
        $1
        ;;
    condrestart|try-restart)
        rh_status_q || exit 0
        restart
        ;;
    *)
        echo $"Usage: $0 {start|stop|status|restart|condrestart|try-restart|reload|force-reload|configtest}"
        exit 2
esac

exit $?
