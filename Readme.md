PiPower
=======

Der Raspberry Pi muss sauber heruntergefahren werden, damit die Daten auf der SD-Karte keinen Schaden nehmen. Auch nach dem Herunterfahren liegt noch Spannung am Pi an und es fließt ein nicht unerheblicher Ruhestrom.

PiPower schafft die Möglichkeit, den Pi mit einem einzigen Tastschalter sauber ein- und auszuschalten. Einschalten stellt dabei kein Problem dar, da der Pi nach dem Einschalten der Stromzufuhr bootet.

Die größere Herausforderung ist das Ausschalten. Hier soll die Stromversorgung nach dem Herunterfahren des Pi getrennt werden. Außerdem soll das softwareseitige Hderunterfahren des Pi erkannt werden und auch danach die Stromzufuhr unterbrochen werden.

Damit sind die Anforderungen umrissen:

- Aus- und Einschalten mit einem Tastschalter
- Visualisierung des Zustands des Pi mit einer LED
- Erkennen von softwareseitigem Herunterfahren
- Trennen der Pi Stromzufuhr nach dem Herunterfahren
- Einleiten von Pi Neustarts mit dem Tastschalter

Da sich mit reinen Ausschaltverzögerungen auf Basis eines Timers wie zum Beispiel einem NE555 nicht alle Anforderungen abdecken lassen, wird das Projekt in zwei Komponenten zerlegt.

Eine Steuerungseinheit auf Basis eines Atmel AVR Microcontrollers, die mit einem Dämon auf dem Pi kommuniziert. Diese Einheit stellt die Benutzerschniitstelle zur Verfügung (Tastschalter), visualisiert den Pi Betriebszustand und steuert den Pi.

Der Dämon auf dem Pi nimmt die Befehle der Steuerungseinheit entgegen und führt diese aus (Shutdown, Reboot). Ausserdem sendet er regelmässig Lebenszeichen. Fallen diese aus, geht die Steuerungseinheit von einem softwareseitigen Herunterfahren des Pi aus und kappt nach einer festgelegten Zeitspanne die Stromzufuhr.
 
