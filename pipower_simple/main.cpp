
#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

/**
 * \mainpage pipower Dokumentation
 *
 * \section intro Einführung
 *
 * Ziel des Projektes war eine Möglichkeit zu schaffen, den Raspberry Pi
 * mit einem Tastschalter sauber ein- und ausschalten zu können.
 * 
 * Nach dem Einschalten der Stromzuführung bootet der Pi und ist danach
 * betriebsbereit. Das Problem ist das Ausschalten mit einer sicheren
 * Trennung von der Betriebsspannung, da der Pi auch nach dem Shutdown
 * noch einen relativ hohen Ruhestrom zieht.
 *
 * \section features Funktionen
 *
 * pipower stellt folgende Funktionen bereit:
 *
 * - Ein- und Ausschalten des Pi mit einem einzigen Tastschalter
 * - Visualisierung des Betriebszustandes mit einer LED
 * - Erkennen eines vom Pi ausgelösten Reboots oder Shutdowns
 * - Vollständige Trennung von der Spannungsversorgung nach dem Shutdown des Pi
 * - Reboot durch langem Tastendruck
 * - Signalgenerator-Funktionalität (Rechtecksignale 0,2Hz, 0,5Hz, 1Hz, 5Hz, 10Hz, 50Hz, 100Hz, 200Hz)
 *
 * \section design Aufbau
 *
 * pipower basiert auf einem Mikrocontroller 8-bit Atmega ATmega328p.
 * Dieser kontrolliert die Spannungsversorgung des Pi und überwacht dessen
 * Betriebszustand.
 *
 * Auf dem Pi läuft ein Hintergrundprogramm, das regelmäßig Signale über eine GPIO-Output-Pin
 * an den ATMega328p sendet. Wenn diese Lebenszeichen des Pi nicht mehr empfangen werden,
 * wird darauf reagiert. Weiterhin überwacht dieses Programm einen GPIO-Input-Pin,
 * an den pipower Steuersignale an den Pi schickt. Damit wird der Neustart und das
 * Herunterfahren des Pi gesteuert. Das Programm auf dem Pi muß dazu mit Rootrechten
 * laufen, um die entsprechenden Systemkommandos ausführen zu können.
 *
 * Nach dem Herunterfahren des Pi befindet sich pipower noch im Stand by Modus.
 * Für einen echten Aus-Zustand zu erreichen, wird das gesamte System anschließend
 * nach einer bestimmten Zeitverzögerung über den Abfall eines Relais vom Netz getrennt.
 *
 * \section modi Betriebsmodi
 *
 * pipower kannn sich in genau einem der folgenden Betriebszustände befinden:
 *
 * -# Stand by
 *    Dies ist bezogen auf pipower der Ruhezustand vor dem Einschalten beziehungsweise
 *    nach dem Herunterfahren des Pi. Die Spannungsversorgung ist ausgeschaltet und die
 *    LED ist aus. Dieser Modus kann durch einen kurzen Tastendruck verlassen werden,
 *    der die Spannungsversorgung des Pi einschaltet und dessen Bootvorgang startet.
 *
 * -# Starting
 *    Es wird eine definierte Zeit auf Lebenszeichen vom Pi gewartet. Die LED blinkt.
 *    Treffen Lebenszeichen vom Pi ein, wird der Modus Starting in Richtung Running
 *    verlassen.
 *
 * -# Running
 *    Dies ist der normale Betriebsmodus des Pi. Die LED leuchtet konstant.
 *    Ein kurzer Tastendruck löst das Herunterfahren und ein langer Tastendruck
 *    einen Neustart des Pi aus. Damit wird der Modus verlassen. Außerdem wird
 *    auf das Ausbleiben der Pi Lebenszeichen ohne Tastendruck reagiert.
 *    Dazu kann es durch Senden der entsprechenden Systemkommandos aus dem Pi
 *    kommen (sudo halt, sudo reboot).
 *
 * -# Shut down
 *    Es werden die entsprechenden Steuerkommandos an die Hintergrundanwendung
 *    im Pi geschickt, die das Herunterfahren oder den Neustart einleiten.
 *    Die LED blinkt. Nach dem Ausbleiben der Pi Lebenszeichen wird der Modus
 *    in Richtung Last Chance verlassen.
 *
 * -# Last Chance
 *    Hier wird eine definierte Zeit auf Pi Lebenszeichen gewartet. Im Fall
 *    eines Neustarts kann der Pi neu booten. Treffen diese Lebenszeichen
 *    nicht ein, wurde der Pi heruntergefahren und die Spannungsversorgung
 *    kann getrennt werden. Die LED blitzt kurz. Der nächste Modus ist
 *    wieder Stand by.
 */

/*
|--------------------------------------------------------------------
| 328p       Uno       | 328p       Uno | 328p       Uno            |
|--------------------------------------------------------------------
| PINB0 <==> D8        | PINC0 <==> A0  | PIND0 <==> D0             |
| PINB1 <==> D9        | PINC1 <==> A1  | PIND1 <==> D1             |
| PINB2 <==> D10       | PINC2 <==> A2  | PIND2 <==> D2 (INT0)      |
| PINB3 <==> D11       | PINC3 <==> A3  | PIND3 <==> D3 (INT1, PWM) |
| PINB4 <==> D12       | PINC4 <==> A4  | PIND4 <==> D4             |
| PINB5 <==> D13 (LED) | PINC5 <==> A5  | PIND5 <==> D5             |
| PINB6                | PINC6          | PIND6 <==> D6             |
| PINB7                |                | PIND7 <==> D7             |
|--------------------------------------------------------------------
*/
#define PIN_FROM_PI         PIND2   // Pulse vom Pi (Lebenszeichen)
#define PIN_PWM             PIND3   // Signalgenerator
#define PIN_LED             PIND4   // Die zentrale LED
#define PIN_TO_PI           PIND5   // Signale an den PI
#define PIN_POWER           PIND6   // Pi-Power
#define PIN_PUSHBUTTON      PIND7   // Der Taster (Schliesser)
/*
----------------------------------------------------------------------
| PIN_DIL_1000 | PIN_DIL_0100 | PIN_DIL_0010 | PIN_DIL_0001 | Freq.Hz|
----------------------------------------------------------------------
|      0       |      0       |      0       |      0       | off    |
|      1       |      0       |      0       |      1       | 0,5    |
|      1       |      0       |      1       |      0       | 1      |
|      1       |      0       |      1       |      1       | 5      |
|      1       |      1       |      0       |      0       | 10     |
|      1       |      1       |      0       |      1       | 50     |
|      1       |      1       |      1       |      0       | 100    |
|      1       |      1       |      1       |      1       | 200    |
----------------------------------------------------------------------
*/
#define PIN_DIL_0001        PINB0   // DIL-Schalter 0
#define PIN_DIL_0010        PINB1   // DIL-Schalter 1
#define PIN_DIL_0100        PINB2   // DIL-Schalter 2
#define PIN_DIL_1000        PINB3   // DIL-Schalter 3

#define PIN_PI_PULSE        PINB4   // Pi-Simulation (muss mit PIN_FROM_PI verbunden sein)
#define PIN_LED_BOARD       PINB5   // LED auf dem Arduino Uno Board

#define PI_CHECK_INTERVAL_SEC           3   // Zeit in Sekunden, in der jeweils die Pi-Pulse gezählt werden
#define PI_CHECK_INTERVAL_PULSE_COUNT   2   // Anzahl der Pulse, die mindestens vom Pi kommen müssen
#define POWER_ON_DELAY_SEC              5   // Maximale Wartezeit auf Pi-Pulse nach dem Einschalten in Sekunden
#define POWER_OFF_DELAY_SEC             5   // Maximale Wartezeit auf Pi-Pulse vor dem Ausschalten in Sekunden

enum class PiState : uint8_t
{
    StandBy     = 0,
    Starting    = 1,
    Running     = 2,
    ShutDown    = 3,
    LastChance  = 4,
};

#define isFirst(state)    stateFirst & _BV((uint8_t)(state))
#define setFirst(state)   stateFirst = 0b00011111; stateFirst &= ~_BV((uint8_t)(state))

enum class LedState : uint8_t
{
    Off,
    On,
    Flashing,
    Blinking,
};



/**
Die Zähler werden wie folgt benutzt:
Timer 0: Key-Debounce, Detektion der Pi-Pulse
Timer 2: Signalgenerator
Timer 1: Steuerung der LED, Ausgabe der Steuersignale an den Pi, Pi-Simulation

                                0,0  0,1  0,2  0,3  0,4  0,5  0,6  0,7  0,8  0,9  1,0
    LED-Steuerung:   Flash       1    0
                     Blink       1                             0
    Pi-Signale                   1         0                             1         0
    Pi-Simulation                1    0    1    0    1    0    1    0    1    0    1
*/


#define COUNTER_TOP_16(prescale,ms)     (uint16_t)(((F_CPU / (prescale)) * (0.001 * (ms))) + 1.0)
#define COUNTER_TOP_8(prescale,ms)      (uint8_t)COUNTER_TOP_16((prescale),(ms))

volatile uint8_t g_timerSeconds = 0;
volatile uint8_t g_piPulses     = 0;
volatile bool    g_piOK         = false;
volatile bool    g_piSimulation = false;
volatile uint8_t g_piSignals    = 0;
volatile uint8_t g_pwmTicksHigh = 0;
volatile uint8_t g_pwmTicksLow  = 0;

LedState g_ledState = LedState::Off;

//=============================================================================
// Key DeBouncing
//=============================================================================

#define REPEAT_MASK              _BV(PIN_PUSHBUTTON)
#define REPEAT_START             100    // after 1000ms
#define REPEAT_NEXT               20    // every 200ms

volatile uint8_t  g_keyState    = 0;
volatile uint8_t  g_keyPress    = 0;
volatile uint8_t  g_keyRepeat   = 0;

//-----------------------------------------------------------------------------
// check if a key has been pressed. Each pressed key is reported only once.
//-----------------------------------------------------------------------------
uint8_t getKeyPress(uint8_t keyMask)
{
    cli();                      // read and clear atomic !
    keyMask &= g_keyPress;       // read key(s)
    g_keyPress ^= keyMask;       // clear key(s)
    sei();
    return keyMask;
}
 
//-----------------------------------------------------------------------------
// check if a key has been pressed long enough such that the
// key repeat functionality kicks in. After a small setup delay
// the key is reported being pressed in subsequent calls
// to this function. This simulates the user repeatedly
// pressing and releasing the key.
//-----------------------------------------------------------------------------
uint8_t getKeyRepeat(uint8_t keyMask)
{
    cli();                      // read and clear atomic !
    keyMask &= g_keyRepeat;     // read key(s)
    g_keyRepeat ^= keyMask;     // clear key(s)
    sei();
    return keyMask;
}

//-----------------------------------------------------------------------------
// check if a key is pressed right now
//-----------------------------------------------------------------------------
uint8_t getKeyState(uint8_t keyMask)
{
    keyMask &= g_keyState;
    return keyMask;
}

//-----------------------------------------------------------------------------
// short key press
//-----------------------------------------------------------------------------
uint8_t getKeyShort(uint8_t keyMask)
{
    cli();                                          // read key state and key press atomic !
    return getKeyPress(~g_keyState & keyMask);
}
 
//-----------------------------------------------------------------------------
// long key press
//-----------------------------------------------------------------------------
uint8_t getKeyLong(uint8_t keyMask)
{
    return getKeyPress(getKeyRepeat(keyMask));
}

//=============================================================================
// Key DeBouncing Ende
//=============================================================================

uint8_t diffSeconds(const uint8_t seconds)
{
    if (g_timerSeconds >= seconds)
        return (g_timerSeconds - seconds);        
    return (0xff - seconds + g_timerSeconds);
}

/**
\fn     uint8_t diffPulses(const uint8_t pulses)
\brief  diffPulses gibt die absolute Anzahl an Pulsen zurück, die der Pi
        gesendet hat. Dazu wird ein Referenzwert übergeben. Überläufe
        werden berücksichtigt. Valide Ergebnisse werden nur geliefert,
        wenn die absolute Differenz zwischen Referenz- und Momentwert
        zwischen 0 und 255 liegt.
\param  Der Referenzwert, zu dem die Differenz an Pulsen gesucht wird.
\return Die gesuchte Differenz.
*/
uint8_t diffPulses(const uint8_t pulses)
{
    if (g_piPulses >= pulses)
        return (g_piPulses - pulses);        
    return (0xff - pulses + g_piPulses);
}

void setPiSimulation(const bool onOff)
{
//    cli();
    g_piSimulation = onOff;
    if (!onOff)
        PORTB &= ~_BV(PIN_PI_PULSE);
//    sei();
}

bool isPiAlive()
{
    return g_piOK;
}

void setLedState(const LedState ledState)
{
//    cli();
    g_ledState = ledState;
//    sei();
}

void pwmCheck()
{
    static uint8_t prevPinB = 0;
    
    // PWM Status hat sich geändert
    if ((PINB & _BV(PIN_DIL_1000)) != (prevPinB & _BV(PIN_DIL_1000)))
    {
        // PWM aus
        if (!(PINB & _BV(PIN_DIL_1000)))
        {
            TCCR2B &= ~(_BV(CS12) | _BV(CS11) | _BV(CS10));
            prevPinB = PINB;
            return;
        }
    }
    
    uint8_t prevPwmMode = (prevPinB ^ 0b00000111);
    uint8_t pwmMode     = (PINB     ^ 0b00000111);

    prevPinB = PINB;

    if (pwmMode == prevPwmMode)
        return;

    TCCR2B &= ~(_BV(CS12) | _BV(CS11) | _BV(CS10));
    
    //----------------------------------------------
    // Prescale   max. Tickfreq.   CS22  CS21  CS20    
    //----------------------------------------------
    // 1024       16,26ms            1     1     1
    //  256        4,06ms            1     1     0
    //  128        2,03ms            1     0     1
    //   64        1,02ms            1     0     0
    //   32        0,51ms            0     1     1
    //    8        0,13ms            0     1     0
    //    1        0,02ms            0     0     1
    //----------------------------------------------
    
    switch (pwmMode)
    {
        case 0b00000000:
        {
            // 0,2Hz ==> 1000ms/4000ms
            // Prescale 1024, Timer 10ms, 100 Ticks (20/80)
            TCCR2B |= _BV(CS12);
            OCR2A   = 100;
            break;
        }
        case 0b00000001:
        {
            // 0,5hz ==> 400ms/1600ms
            // Prescale 1024, Timer 10ms, 
            TCCR2B |= _BV(CS12);
            OCR2A   = 100;
            break;
        }
        case 0b00000010:
        {
            // 1Hz ==> Tick 200ms
            TCCR2B |= _BV(CS11) | _BV(CS10);
            OCR2A   = 100;
            break;
        }
        case 0b00000011:
        {
            // 5Hz ==> Tick 40ms
            TCCR2B |= _BV(CS11) | _BV(CS10);
            OCR2A   = 100;
            break;
        }
        case 0b00000100:
        {
            // 10Hz ==> Tick 20ms
            TCCR2B |= _BV(CS11) | _BV(CS10);
            OCR2A   = 100;
            break;
        }
        case 0b00000101:
        {
            // 50Hz ==> Tick 4ms
            TCCR2B |= _BV(CS11);
            OCR2A   = 100;
            break;
        }
        case 0b00000110:
        {
            // 100Hz ==> Tick 2ms
            TCCR2B |= _BV(CS11);
            OCR2A   = 100;
            break;
        }
        case 0b00000111:
        {
            // 200Hz ==> Tick 1ms
            TCCR2B |= _BV(CS10);
            OCR2A   = 100;
            break;
        }
    }
}

//=============================================================================
// Interrupt-Handlers
//=============================================================================

ISR(TIMER0_COMPA_vect)
{
    //-------------------------------------------------------------------------
    // Key-DeBouncing
    //-------------------------------------------------------------------------
    static uint8_t ct0 = 0xff;
    static uint8_t ct1 = 0xff;
    static uint8_t rpt;
 
//  uint8_t i = g_keyState ^ ~PIND;         // key changed (active low) ?
    uint8_t i = g_keyState ^ PIND;          // key changed (active high) ?

    ct0 = ~(ct0 & i);                       // reset or count ct0
    ct1 = ct0 ^ (ct1 & i);                  // reset or count ct1
    i &= ct0 & ct1;                         // count until roll over ?
    g_keyState ^= i;                        // then toggle debounced state
    g_keyPress |= g_keyState & i;           // 0->1: key press detect
 
    if ((g_keyState & REPEAT_MASK) == 0)    // check repeat function
        rpt = REPEAT_START;                 // start delay
    if (--rpt == 0)
    {
        rpt = REPEAT_NEXT;                  // repeat delay
        g_keyRepeat |= g_keyState & REPEAT_MASK;
    }
    //-------------------------------------------------------------------------
}

/**
Timer 1: Steuerung der LED, Ausgabe der Steuersignale an den Pi, Pi-Simulation

                                0,0  0,1  0,2  0,3  0,4  0,5  0,6  0,7  0,8  0,9  1,0
    LED-Steuerung:   Flash       1    0
                     Blink       1                        0
    Pi-Signale                   1         0                             1         0
    Pi-Simulation                1    0    1    0    1    0    1    0    1    0    1
*/
ISR(TIMER1_COMPA_vect)
{
    static uint8_t tick100ms = 0;
    if (++tick100ms > 10)
    {
        tick100ms = 0;
        if (g_timerSeconds == 0xff)
            g_timerSeconds = 0;        
        g_timerSeconds++;
//        PORTB ^= _BV(PIN_LED_BOARD);
        
        // Der Pi muss in einem bestimmten Zeitraum eine Mindestanzahl Pulse
        // geschickt haben, um als lebend zu gelten.
        static uint8_t piCheckPulses  = 0;
        if ((g_timerSeconds % PI_CHECK_INTERVAL_SEC) == 0)
        {
            g_piOK = (diffPulses(piCheckPulses) > PI_CHECK_INTERVAL_PULSE_COUNT);
            piCheckPulses = g_piPulses;
//            if (g_piOK)
//                PORTD |= _BV(PIN_PWM);
//            else
//                PORTD &= ~_BV(PIN_PWM);
        }
    }
    
    if (g_piSimulation)
        PORTB ^= _BV(PIN_PI_PULSE);
    else
        PORTB &= ~_BV(PIN_PI_PULSE);
    
    switch (tick100ms)
    {
        case 0:
        {
            switch (g_ledState)
            {
                case LedState::Off: 
                    PORTD &= ~_BV(PIN_LED); 
                    break;
                case LedState::On:
                case LedState::Flashing:
                case LedState::Blinking:
                    PORTD |= _BV(PIN_LED);
                    break;
            }
            
            if (g_piSignals > 0)
                PORTD |= _BV(PIN_TO_PI);

            break;
        }
        
        case 1:
        {
            if (g_ledState == LedState::Flashing)
                PORTD &= ~_BV(PIN_LED);
            break;            
        }
        
        case 2:
        {
            if (g_piSignals > 0)
            {
                PORTD &= ~_BV(PIN_TO_PI);
                g_piSignals--;
            }
            break;            
        }
        
        case 5:
        {
            if (g_ledState == LedState::Blinking)
                PORTD &= ~_BV(PIN_LED);
            break;                        
        }

        case 8:
        {
            if (g_piSignals > 0)
                PORTD |= _BV(PIN_TO_PI);
            break;                        
        }
            
        case 10:
        {
            if (g_piSignals > 0)
            {
                PORTD &= ~_BV(PIN_TO_PI);
                g_piSignals--;
            }
            break;                        
        }
    }
}

ISR(TIMER2_COMPA_vect)
{
    static uint8_t tick = 0;
//    static bool pwmHigh = false;
    
    if (++tick == 100)
    {
        tick = 0;
//        PORTD ^= _BV(PIN_PWM);    
    }
    if (tick == 0)
        PORTD |= _BV(PIN_PWM);    
    if (tick == 40)
        PORTD &= ~_BV(PIN_PWM);
}

ISR(INT0_vect)
{
    // Überlauf Pulsezähler abfangen 
    if (g_piPulses == 0xff)
        g_piPulses = 0;    
    g_piPulses++;
    
    PORTB ^= _BV(PIN_LED_BOARD);
}

//=============================================================================
// Main
//=============================================================================

int main(void)
{
    // Output Pins ==> low
    DDRD  |=  _BV(PIN_PWM) | _BV(PIN_LED) | _BV(PIN_TO_PI) | _BV(PIN_POWER);
    PORTD &= ~(_BV(PIN_PWM) | _BV(PIN_LED) | _BV(PIN_TO_PI) | _BV(PIN_POWER));
    DDRB  |=  _BV(PIN_PI_PULSE) | _BV(PIN_LED_BOARD);
    PORTB &= ~(_BV(PIN_PI_PULSE) | _BV(PIN_LED_BOARD));

    // Input Pins
    DDRD  &= ~_BV(PIN_FROM_PI);
    DDRB  &= ~(_BV(PIN_DIL_0001) | _BV(PIN_DIL_0010) | _BV(PIN_DIL_0100) | _BV(PIN_DIL_1000));
    // Pull up Widerstände an
    PORTB |= _BV(PIN_DIL_0001) | _BV(PIN_DIL_0010) | _BV(PIN_DIL_0100) | _BV(PIN_DIL_1000);
    
    // Timer 0: 100Hz
    TCCR0A |= _BV(WGM01);
    OCR0A  |=  COUNTER_TOP_8(1024,10);
    TIMSK0 |= _BV(OCIE0A);
    TCCR0B |= _BV(CS02) | _BV(CS00);
    
    // Timer 1: 10Hz
    TCCR1B |= _BV(WGM12);
    OCR1A  |=  COUNTER_TOP_16(1024,100);
    TIMSK1 |= _BV(OCIE1A);
    TCCR1B |= _BV(CS12) | _BV(CS10);

    // Timer 2: 100Hz
    TCCR2A |= _BV(WGM21);
    OCR2A  |=  COUNTER_TOP_8(1024,10);
    TIMSK2 |= _BV(OCIE2A);
    TCCR2B |= _BV(CS22) | _BV(CS21) | _BV(CS20);

    // Externer Interrupt 0, steigende Flanke: hier kommen die Pi-Pulse an.
    EICRA |= _BV(ISC01) | _BV(ISC00);
    EIMSK |= _BV(INT0);

    sei();

    PiState piState      = PiState::StandBy;
    uint8_t stateFirst   = 0b00011111;
    uint8_t stateSeconds = 0;
    
    while (true)
    {
        switch (piState)
        {
            //-----------------------------------------------------------------
            // Stand by
            //-----------------------------------------------------------------
            case (PiState::StandBy):
            {
                if (isFirst(PiState::StandBy))
                {
                    PORTD &= ~_BV(PIN_POWER);
                    setPiSimulation(false);
                    setLedState(LedState::Off);                    
                    setFirst(PiState::StandBy);
                }
                
                if (getKeyShort(_BV(PIN_PUSHBUTTON)))
                    piState = PiState::Starting;

                break;
            }

            //-----------------------------------------------------------------
            // Starting
            //-----------------------------------------------------------------
            case (PiState::Starting):
            {
                if (isFirst(PiState::Starting))
                {
                    PORTD |= _BV(PIN_POWER);
                    setLedState(LedState::Blinking);
                    setPiSimulation(true);
                    stateSeconds = g_timerSeconds;
                    setFirst(PiState::Starting);
                }
                
                if (diffSeconds(stateSeconds) < POWER_ON_DELAY_SEC)
                {
                    if (isPiAlive())
                        piState = PiState::Running;
                }
                else
                {
                    piState = PiState::LastChance;
                }
                
                break;
            }

            //-----------------------------------------------------------------
            // Running
            //-----------------------------------------------------------------
            case (PiState::Running):
            {   
                if (isFirst(PiState::Running))
                {
                    setLedState(LedState::On);
                    setFirst(PiState::Running);
                }
                
                if (getKeyShort(_BV(PIN_PUSHBUTTON)))
                    g_piSignals = 1;                
                if (getKeyLong(_BV(PIN_PUSHBUTTON)))
                    g_piSignals = 2;
                
                if ((g_piSignals != 0) || !isPiAlive())
                {                    
                    piState = PiState::ShutDown;
                }
                
                break;
            }
            
            //-----------------------------------------------------------------
            // Shut down
            //-----------------------------------------------------------------
            case (PiState::ShutDown):
            {
                if (isFirst(PiState::ShutDown))
                {
                    setLedState(LedState::Blinking);
                    setPiSimulation(false);
                    setFirst(PiState::ShutDown);
                }                
                
                if (!isPiAlive())
                    piState = PiState::LastChance;

                break;
            }

            //-----------------------------------------------------------------
            // Last chance
            //-----------------------------------------------------------------
            case (PiState::LastChance):
            {
                if (isFirst(PiState::LastChance))
                {
                    setLedState(LedState::Flashing);
                    stateSeconds = g_timerSeconds;
                    setFirst(PiState::LastChance);
                }
                
                if (diffSeconds(stateSeconds) < POWER_OFF_DELAY_SEC)
                {
                    if (isPiAlive())
                        piState = PiState::Running;
                }
                else
                {
                    piState = PiState::StandBy;
                }
                
                break;
            }
        }
    }
    
    return 0;
}
